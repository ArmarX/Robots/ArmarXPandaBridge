/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXPandaBridge::ArmarXObjects::PandaUnit
 * @author     Mirko Wächter ( mirko dot waechter at kit dot edu )
 * @author     Christian Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#define BOOST_TEST_MODULE ArmarXPandaBridge::ArmarXObjects::PandaUnit


#define ARMARX_BOOST_TEST


// STD/STL
#include <iostream>

// PandaX
#include <ArmarXPandaBridge/Test.h>
#include <ArmarXPandaBridge/components/PandaUnit/PandaUnit.h>
using namespace pandax;


BOOST_AUTO_TEST_CASE(testExample)
{
    PandaUnit instance;

    BOOST_CHECK_EQUAL(true, true);
}
